// ignore_for_file: public_member_api_docs, sort_constructors_first

part of 'todo_list_cubit.dart';

class TodoListState extends Equatable {
  final List<Todo> todos;
  const TodoListState({
    required this.todos,
  });

  factory TodoListState.initial() {
    return const TodoListState(todos: []);
  }

  @override
  List<Object> get props => [todos];

  @override
  String toString() => 'TodoListState(todos: $todos)';

  TodoListState copyWith({
    List<Todo>? todos,
  }) {
    return TodoListState(
      todos: todos ?? this.todos,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};
    final Map<String, dynamic> allTodos = {
      'todos': todos.map((x) => x.toMap()).toList()
    };
    result.addAll(allTodos);
    return result;
  }

  factory TodoListState.fromMap(Map<String, dynamic> map) {
    return TodoListState(
      todos: List<Todo>.from(
        (map['todos']).map<Todo>(
          (x) => Todo.fromMap(x as Map<String, dynamic>),
        ),
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory TodoListState.fromJson(String source) =>
      TodoListState.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool get stringify => true;
}
